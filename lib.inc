section .text
 
 
exit: 
    mov rax, 60
    syscall

string_length:
  xor rax, rax
  .loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop
  .end:
    ret

print_string:
  push rdi                
  call string_length
  mov rdx, rax           
  pop rsi               
  mov rdi, 1  
  mov rax, 1    
  syscall
  ret

print_char:
  push rdi
  mov rsi, rsp
  mov rdi, 1
  mov rax, 1
  mov rdx, 1
  syscall
  pop rdi
  ret

print_newline:
  xor rax, rax
  mov rdi, 0x0A
  call print_char
  ret

print_uint:
  mov r8, 10
  push rbp
  mov rbp, rsp
  push 0
  mov rsi, rsp
  sub rsp, 20
  mov rax, rdi
  .loop:
    xor rdx, rdx
    div r8
    add dl, '0'
    dec rsi
    mov [rsi], dl
    test rax, rax
    jnz .loop
  mov rdi, rsi
  call print_string
  mov rsp, rbp
  pop rbp
  ret


print_int:
  test   rdi, rdi
  jge   print_uint
  push  rdi
  mov   rdi, 45
  call  print_char
  pop   rdi
  neg   rdi
  jmp print_uint



string_equals:
  mov rcx, -1
  mov rax, 1
  .loop:
    inc rcx
    mov r8b, byte[rdi+rcx]
    mov r9b, byte[rsi+rcx]
    cmp r8b, r9b
    jz .ret
    cmp r8b, r9b
    je .loop
  xor rax, rax
  .ret:
    cmp r8b, r9b
    je .end
    xor rax, rax
  .end:
    ret
    


read_char:
  xor rax, rax
  xor rdi, rdi
  dec rsp
  mov rsi, rsp
  mov rdx, 1
  syscall
  test rax, rax
  jz .eof
  mov al, [rsp]
  jmp .end
  .eof:
  xor rax, rax
  .end:
  inc rsp
  ret



read_word:
  mov r8, rdi
  mov r9, rsi
  xor rcx, rcx
  .loop:
    push rcx
    push r8
    push r9
    call read_char
    pop r9
    pop r8
    pop rcx       
    cmp rax, ' '
    je .check_to_stop_read
    cmp rax, `\t`
    je .check_to_stop_read
    cmp rax, `\n`
    je .check_to_stop_read
    cmp rax, 0
    je .add_null_and_end
    cmp rax, 0x3 ; ctrl-D ?
    je .add_null_and_end 
    mov byte [r8 + rcx], al
    inc rcx
    cmp rcx, r9
    je .end
    jmp .loop    
  .check_to_stop_read:
    cmp rcx, 0
    je .loop   
  .add_null_and_end:
    mov byte [r8 + rcx], 0
    mov rax, r8
    mov rdx, rcx
    ret
  .end:
    xor rax, rax
    ret


 

parse_uint:
  xor   rcx, rcx
  xor   rax, rax
  xor   rdx, rdx
  .loop:
    cmp byte[rdi+rdx], 48
    jl .exit
    cmp byte[rdi+rdx], 57
    jg .exit
    mov cl, [rdi+rdx] 
    sub cl, 48
    imul rax, 10   
    add rax, rcx
    inc rdx 
    jmp .loop
  .exit:
    ret

parse_int:
  xor   rax, rax
  cmp   byte[rdi], 45
  jne   parse_uint
  inc   rdi
  call  parse_uint
  neg   rax
  inc   rdx
  ret 



string_copy:
  xor rcx, rcx
  .loop:
    mov al, byte[rdi]
    inc rdi
    inc rcx        
    cmp rcx, rdx         
    jg .fail                
    mov byte[rsi], al         
    inc rsi
    cmp al, 0                 
    je .success
    jmp .loop
  .fail:
    mov rax, 0                     
    jmp .end
  .success:
    mov rax, rcx   
  .end:
    ret
